package criancasbrincando;

import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Equipe Hugaleno, Stéphanie e Vanessa
 */
public class Crianca extends Thread {

    private final Inicio inicio;
    private final Informacoes info;
    private int id = 0;
    private final Semaphore full;
    private final Semaphore empty;
    private String nome;
    private boolean temBola = false;
    private int tempoBrincando = 10;
    private int tempoQuieto = 10;

    public String getNome() {
        return nome;
    }

    public void setTemBola(boolean temBola) {
        this.temBola = temBola;
    }

    public int getTempoBrincando() {
        return tempoBrincando;
    }

    public int getTempoQuieto() {
        return tempoQuieto;
    }

    public boolean temBola() {
        return temBola;
    }

    public Crianca(Inicio inicio, Informacoes info,
            String nome, boolean tembola, int tb, int tq, Semaphore full, Semaphore empty) {
        super(nome);
        this.inicio = inicio;
        this.info = info;
        this.id = ++Constantes.id;
        this.temBola = tembola;
        this.tempoBrincando = tb;
        this.tempoQuieto = tq;
        this.full = full;
        this.empty = empty;

    }

    private void setImagem(String imgPath, String status) {  //Define informações das crianças
        switch (this.id) {
            case 1:
                info.setLblImC1(imgPath);
                info.setLblTxtIm1(status);
                info.setLblNome1(this.getName());
                break;
            case 2:
                info.setLblImC2(imgPath);
                info.setLblTxtIm2(status);
                info.setLblNome2(this.getName());
                break;
            case 3:
                info.setLblImC3(imgPath);
                info.setLblTxtIm3(status);
                info.setLblNome3(this.getName());
                break;
            case 4:
                info.setLblImC4(imgPath);
                info.setLblTxtIm4(status);
                info.setLblNome4(this.getName());
                break;
            case 5:
                info.setLblImC5(imgPath);
                info.setLblTxtIm5(status);
                info.setLblNome5(this.getName());
                break;
            case 6:
                info.setLblImC6(imgPath);
                info.setLblTxtIm6(status);
                info.setLblNome6(this.getName());
                break;
            case 7:
                info.setLblImC7(imgPath);
                info.setLblTxtIm7(status);
                info.setLblNome7(this.getName());
                break;
            case 8:
                info.setLblImC8(imgPath);
                info.setLblTxtIm8(status);
                info.setLblNome8(this.getName());
                break;
            case 9:
                info.setLblImC9(imgPath);
                info.setLblTxtIm9(status);
                info.setLblNome9(this.getName());
                break;

        }
    }

    private void setTempo(String time) {  //Define informações das crianças
        switch (this.id) {
            case 1:

                info.setLblTC1(time);

                break;
            case 2:
                info.setLblTC2(time);
                break;
            case 3:
                info.setLblTC3(time);
                break;
            case 4:
                info.setLblTC4(time);
                break;
            case 5:
                info.setLblTC5(time);
                break;
            case 6:
                info.setLblTC6(time);
                break;
            case 7:
                info.setLblTC7(time);
                break;
            case 8:
                info.setLblTC8(time);
                break;
            case 9:
                info.setLblTC9(time);
                break;

        }
    }

    private void setImCapacidade() {  //Define informações do cesto

        switch (full.availablePermits()) {
            case 0:
                info.setLblImCapCesto("Imagens/c0.JPG");

                break;
            case 1:
                info.setLblImCapCesto("Imagens/c1.JPG");

                break;
            case 2:
                info.setLblImCapCesto("Imagens/c2.JPG");

                break;
            case 3:
                info.setLblImCapCesto("Imagens/c3.JPG");

                break;
            case 4:
                info.setLblImCapCesto("Imagens/c4.JPG");

                break;
            case 5:
                info.setLblImCapCesto("Imagens/c5.JPG");

                break;
            case 6:
                info.setLblImCapCesto("Imagens/c6.JPG");

                break;
            case 7:
                info.setLblImCapCesto("Imagens/c7.JPG");

                break;
            case 8:
                info.setLblImCapCesto("Imagens/c8.JPG");

                break;
            case 9:
                info.setLblImCapCesto("Imagens/c9.JPG");

                break;
            case 10:
                info.setLblImCapCesto("Imagens/c10.JPG");

                break;

        }
    }
//Processo devia ficar ativo durante todo o tempo de descanso.

    public void descansando() {
        long tempoDecorrido;
        inicio.addMensagemStatus(getName() + " descansando.");
        this.setImagem("Imagens/descansando.jpg", "Descansando");
        Date data = new Date();

        long oldTime = data.getTime();
        do {
            tempoDecorrido = (new Date().getTime() - oldTime) / 1000;
            this.setTempo(tempoDecorrido + "s");

        } while (tempoQuieto >= tempoDecorrido);
    }

    public void pegarBola() {
        inicio.addMensagemStatus(getName() + " pegou bola.");
        this.setTempo("");
        this.temBola = true;
    }

    //Processo devia ficar ativo durante todo o tempo.
    public void brincando() {
        long tempoDecorrido;
        inicio.addMensagemStatus(getName() + " brincando.");
        this.setImagem("Imagens/brincando.jpg", "Brincando");
        Date data = new Date();

        long oldTime = data.getTime();
        do {
            tempoDecorrido = (new Date().getTime() - oldTime) / 1000;
            this.setTempo(tempoDecorrido + "s");
        } while (tempoBrincando >= tempoDecorrido);
    }

    public void devolverBola() {
        inicio.addMensagemStatus(getName() + " devolveu bola.");
        this.setTempo("");
        this.temBola = false;
    }

    public void mostra() {
        String statusBola;
        if (this.temBola) {
            statusBola = "Com bola!";
        } else {
            statusBola = "Sem bola!";
        }
        inicio.addMensagemValoresIniciais("Criança: " + getName() + "\n"
                + "Identificador:" + this.id + "\n"
                + statusBola + "\n"
                + "Tempo brincando: " + this.tempoBrincando + "\n"
                + "Tempo descansando: " + this.tempoQuieto + "\n");
    }

    @Override
    public void run() {
        mostra();
        inicio.setVisible(true);
        while (true) {
            setImCapacidade();
            //Toda crianca comeca descansando
            descansando();

            //Se a criança não tem bola ela deve procurar uma no cesto primeiro.
            if (!temBola()) {
                try {
                    //Se o cesto estiver vazio, crianca deve aguardar bola
                    if (full.availablePermits() == 0) {
                        inicio.addMensagemStatus(getName() + " aguardando bola.");
                        this.setImagem("Imagens/aguardando.jpg", "Aguard. bola");
                        this.setTempo("");
                    }
                    //Se houver bola no cesto, crianca pega a bola do 
                    //cesto e vai brincar,caso contrário a crianca dorme até que
                    //apareça uma bola no cesto.

                    full.acquire();

                    //Altera a imagem da capacidade do cesto.
                    setImCapacidade();
                    pegarBola();
                    //Quando a criança pega a bola do cesto ele avisa as crianças
                    //que estão esperando espaço no mesmo pra devolverem a bola.
                    empty.release();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Crianca.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //Criança vai brincar de acordo com o tempo determinado.
            brincando();
            try {

                if (empty.availablePermits() == 0) {
                    inicio.addMensagemStatus(getName() + " aguardando lugar vazio no cesto.");
                    this.setImagem("Imagens/devolvendo.jpg", "Cesto cheio");
                    this.setTempo("");
                }
                //Se o cesto estiver cheio, criança deve aguardar espaço para
                //devolver a bola
                empty.acquire();

                //Criança devolve a bola.
                devolverBola();

                //Ao devolver a bola a criança acorda as outras que estão aguardando
                //uma bola para brincar.
                full.release();

            } catch (InterruptedException ex) {
                Logger.getLogger(Crianca.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
