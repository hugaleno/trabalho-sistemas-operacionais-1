/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import criancasbrincando.Crianca;
import criancasbrincando.AdicionarCrianca;
import criancasbrincando.Informacoes;
import criancasbrincando.Inicio;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Semaphore;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Equipe Hugaleno, Stéphanie e Vanessa
 */
public class GerenciaCriancas {

    private final Inicio inicio;
    private final Informacoes info = new Informacoes();
    ;
    private AdicionarCrianca adicionarCrianca;
    private final Semaphore full;
    private Semaphore empty;

    public GerenciaCriancas(Inicio inicio) {
        this.inicio = inicio;
        this.full = new Semaphore(0);
        this.empty = new Semaphore(inicio.getCapacidadeCesto());
        inicio.addButtonAddListener(new addCriancaListenner());
        inicio.addSliderListener(new SLiderListener());
    }

    private class SLiderListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider) e.getSource();
            if (source.getValueIsAdjusting()) {
                inicio.setSlCapacidade();
                empty = new Semaphore(inicio.getCapacidadeCesto());
            }
        }

    }

    private class CriaCriancaListenner implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            Crianca nc = new Crianca(inicio, info,
                    adicionarCrianca.getTxtNome(),
                    adicionarCrianca.isComBola(),
                    adicionarCrianca.getTempoBrincando(),
                    adicionarCrianca.getTempoDescansando(),
                    full, empty);
            nc.start();
            if (info == null || !info.isVisible()) {
                info.setVisible(true);
            }
        }
    }

    private class addCriancaListenner implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (adicionarCrianca == null || !adicionarCrianca.isVisible()) {
                adicionarCrianca = new AdicionarCrianca(inicio, true);
                adicionarCrianca.addActionPerformed(new CriaCriancaListenner());
                adicionarCrianca.setVisible(true);
            }
        }
    }

}
